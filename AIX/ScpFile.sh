###################################################################################################
#          Program Name  : ScpFile.sh                                                              #
#          Functionality : Copy FILE from source to destination host, and append  status to a file #
#          Created By    : Chirgal Hansdah				                                           #
#	       Creation Date : 13 May 2015         					                                   #
#          Last Updated  : 18 Aug 2015                                                             #
#          Input Paramters: 5                                                                      #
#           $1=SFTP USER                                                                           #
#           $2=SFTP USER                                                                           #
#           $3=REMOTE SOURCE FILE                                                                  #
#           $4=LOCAL DESTINATION FILE                                                              #
#           $5=LOCAL LOG FILE                                                                      #
####################################################################################################
                                                                              

#!/bin/sh
scp ${1}@${2}:${3} ${4}
if [ "$?" -eq 0 ]; 
then 
  echo "File Copy Completed" >> ${5}
  exit 0
else
  echo "File Copy Failed"
  exit 1
fi