##################################################################################
#          Program Name  : TransformFile.sh                                      #
#          Functionality : Delete null char & split single line SFTP file .      #
#          Created By    : Chirgal Hansdah				         #
#	   Creation Date : 10 Nov 2014         					 #
#          Last Updated  : 27 Nov 2014                                           #
#          Input Paramters: 4                                                    #
##################################################################################

#!/bin/sh
result=`ssh -o BatchMode=yes -o ConnectTimeout=5 $1@$2 "perl -i -pe 'tr /\0//d' $3;perl -i -ple 'BEGIN{ $/ = \10066 }' $3;perl -i -pe 's/^\n//' $3" `
if [ $? = 0 ] 
then
   echo "FILE $3 TRANSFORMATION COMPLETED."
   exit 0
else
 echo "FILE $3 TRANSFORMATION FAILED."
 exit -1
fi
