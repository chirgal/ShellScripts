#!/usr/bin/ksh
################################################################################
## istool import Utility                                                      ##
## Import all the .isx files for a specific project                           ##
## Exit 0 Success, >0 Error                                                   ##
################################################################################

. /apps/ETL/IS/8.5/Server/DSEngine/dsenv

proj_name=$1
fileset_name=$2

project_name=`echo $proj_name | tr '[a-z]' '[A-Z]'`

. /projects/ETL/COMMON/istool/environment.cfg

deploy_date=`date '+%Y-%m-%d-%H%M%S'`

project_location="/projects/ETL/"

project_folder=$project_location/$project_name

proj_grp_owner=`ls -ld $project_folder | tr -s " " | cut -d" " -f4`

chgrp -R dstage $project_folder

istool_fileset="/work/TAE/istool_fileset/${fileset_name}_${proj_name}_${deploy_date}"

Log_file_name="/work/TAE/source_import/${fileset_name}_${project_name}_import_${deploy_date}.log"

if [ -f "$Log_file_name" ]; then
	echo "Log file Exist"
else
	touch $Log_file_name
fi

k=0;l=0

if [ `ps -eo pid,comm|grep dsrpcd|grep -vc grep` -eq 1 ] ; then

	echo "######################################################################################################" >> $Log_file_name
	echo " Import Process Started for the project ${vip_name}/${project_name} @ `date` in the server `hostname`" >> $Log_file_name
	echo "######################################################################################################" >> $Log_file_name
	echo >> $Log_file_name

        d1=`ls -arlt /work/$project_name/source_import/${fileset_name}/Scripts/*.isx | tr -s " " | grep "^-" | cut -d" " -f3,9- | grep "^root" | cut -d" " -f2- | wc -l | awk '{print $1}'`
	
        for jobname in `ls -l /work/$project_name/source_import/${fileset_name}/Scripts/*.isx | tr -s " " | grep "^-" | cut -d" " -f3,9- | grep "^root" | cut -d" " -f2-`
	do
		import="import -replace -domain $domain_name:443 `cat /projects/ETL/TAE/deployscripts/connection.txt` -archive '$jobname' -datastage '$vip_name/$project_name';"
	        set +x
	        echo $import > $istool_fileset
	
		i="/apps/ETL/IS/8.5/Clients/istools/cli/istool -script $istool_fileset -v"
		eval "$i" >> $Log_file_name
		
		J=`echo $?`
        	case $J in
	                0)
	                        k=`expr $k + 1`
	                        rm -f $istool_fileset
	                        echo "Removing the job $jobname"
				rm -f $jobname
	                ;;
	                *)
	                        l=`expr $l + 1`
	                        rm -f $istool_fileset
	                ;;
        	esac
	done
	echo "Total Number of Jobs: $d1 ; Jobs Imported : $k; Jobs failed : $l"
	if [ $d1==$k ] ; then
		echo "Removing the Deployment Folder" >> $Log_file_name
	#	rm -R /work/$project_name/source_import/${fileset_name}
	else
		echo "Deployment folder is not removed" >> $log_file_name
	fi
	chown -R dsadm:$proj_grp_owner $project_folder
	echo >> $Log_file_name
	echo "###################################################################" >> $Log_file_name
	echo "                  Import Result                                    " >> $Log_file_name
	echo "###################################################################" >> $Log_file_name
	echo >> $Log_file_name
	echo "Total Objects Imported : " $k >> $Log_file_name
	echo "Total Objects Failed : " $l >> $Log_file_name
else
	sleep 60
	echo "dsrpcd process for $vip_name is not running on `hostname`"
fi

