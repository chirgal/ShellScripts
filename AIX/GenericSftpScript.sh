##################################################################################
#          Program Name  :Generic SFTP Script                                    #
#          Functionality : Transfer Files to a Remote Location through FTP.      #
#          Created By    : Aneesh Menon						 #
#	   Creation Date : 06 April 2006					 #
#          Last Updated  : 27 Nov 2014                                           #
#          Input Paramters: 5                                                    #
##################################################################################
cd $1 #Change directory to path where the files for transfer are kept

if [[ $? -eq 0 ]]  # Checking Validity of path specified in $1
then

#################################################################################
#                                FTP Logic                                      #
#################################################################################
#!/usr/bin/sh
#ftp -v -n "$2" << !!! 
#user "$3" "$4"
#cd $5
#$6 $7
#bye
#!!!
#################################################################################
#                                SFTP Logic                                     #
#################################################################################

sftp "$3@$2" << !!!    ##USER@HOST
cd $5
$6 $7
bye
!!!
##################################################################################
else
echo "!!!      INVALID  FILE PATH      !!!"
exit 1
fi
exit