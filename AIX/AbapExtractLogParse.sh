##################################################################################
#         Program Name  : AbapExtractLogParse.sh                                  #
#         Functionality : Check if ABAP extract stage is executed successfully,   #
#                         when FTP port is closed.                                #
#         Created By    : Chirgal Hansdah				                          #
#	      Creation Date : 10 Nov 2014         					                  #
#         Last Updated  : 27 Nov 2014                                             #
#         Input Paramters: 3                                                      #
#         $1:ProjectName                                                          #
#         $2:JobName[.invocationID]                                               #
#         $3:logfile                                                              #
##################################################################################
#!/bin/sh
x=`$DSHOME/bin/dsjob -logsum -type INFO $1 $2 >$3`
result=`cat $3 |sed '/^[0-9]/d'|tail -r|sed -n '1,/Parallel job initiated/p'|grep 'FTP in progress'|wc -l`
if [ $result = 0 ];
  then 
    echo "AbapExtractStage Failure in job $2" > $3
elif [ $result = 1 ];
  then 
    echo "AbapExtractStage Success in job $2" > $3
fi
chmod 777 $3
exit $result