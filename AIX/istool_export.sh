#!/usr/bin/ksh
################################################################################
## Interactive istool export Utility	                                      ##  
## Exports Project or Folder/Category or Individual Job 	              ##
## Exit 0 Success, >0 Error                                                   ##
################################################################################

########################################################################
# Function to get the User Inputs	       	                      ##	
########################################################################
function f_get_values {
print -n "Enter user ID: "
read user_id
print -n "Enter Password: "
stty -echo
read password_user
stty echo
echo

print -n "Enter DataStage Project Name: "
read proj_name
echo

project_name=`echo $proj_name | tr '[a-z]' '[A-Z]'`

}

#########################################################################
## Function to create the directory for export file set creation       ##
#########################################################################
function f_directory_create {
cd ~${user_id}
rm -rf .eclipse
user_home_dir=`pwd`
user_dir=istool_export

if [ -d $user_dir ]; then
	chmod 700 istool_export
else
	mkdir istool_export
	chmod 700 istool_export
fi

}

#########################################################################
## Function to create the directory for export file set creation       ##
#########################################################################
function f_logfile_create {

Log_file_name="/work/TAE/Istool_Export_log/${project_name}_export_${export_date}.log"

if [ -f "$Log_file_name" ]; then
	echo "Log file Exist"
else
	touch $Log_file_name
	echo "Log file $Log_file_name created"
fi
}

#########################################################################
## Function to set variables                                           ##
#########################################################################
function f_set_fileset {
export_date=`date '+%Y-%m-%d-%H%M%S'`
export_home="$user_home_dir/istool_export"
fileset_name="${export_home}/${project_name}_${export_date}"

}

#########################################################################
## Function to Display headings                                        ##
#########################################################################
function f_menu_header {

echo ""
echo ""
echo "########################################################"
echo "             $1                                         "
echo "########################################################"
echo ""
echo "$2"
echo ""

}

#########################################################################
# Function to Display the Export Result                                ##
#########################################################################

function f_output_result {

echo "######################################################"
echo "                  Export Result                       "
echo "######################################################"
echo
echo "Total Objects Exported : " $k
echo "Total objects Skipped : " $l
echo "See the failed.cfg file located in your project folder /work/${project_name}/source_export/ for failed componenets"

}

#########################################################################
# Function to export the project or folder or job                      ##
#########################################################################
function f_export {
clear
f_menu_header "istool Export Utility" "Please select a Export Type"

PS3='Select an option and press Enter: '
OPTS=Category" "Project" "IndividualJob" "QUIT
select i in $OPTS
do
if [[ $i = QUIT ]] ; then
    break
elif [[ -z "$i" ]]; then
    f_menu_header "istool Export Utility" "Please select a Valid option"
else
   case $i in
	Category)
		f_get_values
		f_directory_create
                f_set_fileset
                f_logfile_create
		echo 
		echo
		echo "Domain Name is : " $domain_name:443
 	       	echo "Export Project Name is :" $project_name
 	       	echo "Updated Config file location : /work/$project_name/source_export/category.cfg" 
 	       	echo
 	       	echo
		echo "Confirm the above values are correct before Category Export begins (Y/N): "
		read YN
		case $YN in
			[yY]*)
				k=0;l=0
				cat /work/$project_name/source_export/category.cfg | while read category
				do
					exportjob="-base=\"$vip_name/$project_name\" \"$category/*/*.*\""
					category_path_archive=`echo $category | rev | cut -d/ -f1 | rev | tr ' ' '\_' | tr '/' '\_'`
        				archivepath="/work/$project_name/source_export/$category_path_archive.isx"
        				archivelog="/work/$project_name/source_export/$category_path_archive.log"

 	       				print ""
 	       				i="export -domain $domain_name:443 -u $user_id -p $password_user -archive "$archivepath" -datastage '-incexec $exportjob';"

					set +x
					echo $i > $fileset_name
					
					s2="/apps/ETL/IS/8.5/Clients/istools/cli/istool -script $fileset_name"
					echo "Exporting the Category $category"
					eval "$s2" >> $Log_file_name
					

 	        			J=`echo $?`
        				case $J in
                				0)
                        				k=`expr $k + 1`
                        				echo "Export completed successfully for the category: $category"
                        				
                				;;
                				*)
                        				l=`expr $l + 1`
                        				echo "Export failed for the category: $category"
                        				failed_config="/work/${project_name}/source_export/Category_export_failed_${export_date}.cfg"
                        				echo $jobname >> $failed_config
                				;;
        				esac

				done
				ERR=$?
				if [[ $ERR -ne 0 ]] ; then
				{
					print "Failed to Export the folder, see errors above."
				exit 1
				}
				fi
			;;
			*)
  				f_menu_header "Category Export" "Update the Category Config file and Select a Valid option"
  				REPLY=''
  				continue
     			;;
		esac
	;;
	Project)
        	f_get_values
        	f_directory_create
                f_set_fileset
                f_logfile_create
        	echo "Domain Name is : " $domain_name:443
 	       	echo "Export Project Name is :" $project_name
 	       	echo
 	       	echo
 	       	echo "Confirm before the Entire project export Begins (Y/N): "
		read YN
		case $YN in
			[yY]*)
                                k=0;l=0
				exportjob="$vip_name/$project_name/*/*.*"
                		project_path_archive=`echo $project_name | tr ' ' '\_'`
                		archivepath="/work/$project_name/source_export/$project_path_archive.isx"
 				archivelog="/work/$project_name/source_export/$project_path_archive.log"
				
                		print ""
                		i="export -domain $domain_name:443 -u $user_id -p $password_user -archive "$archivepath" -datastage '-incexec \"$exportjob\"';"

				set +x
				echo $i > $fileset_name
				
				s2="/apps/ETL/IS/8.5/Clients/istools/cli/istool -script $fileset_name"
				echo "Exporting the Project $project_name"
				eval "$s2" >> $Log_file_name

                		J=`echo $?`
                		case $J in
                        		0)
                                		k=`expr $k + 1`
                        		;;
                        		*)
                                		l=`expr $l + 1`
                       			 ;;
                		esac
			;;
			*)
  				f_menu_header "Project Export" "Select a valid option"
				REPLY=''
   				continue
   			;;
		esac
	;;
	IndividualJob)
		f_get_values
		f_directory_create
                f_set_fileset
                f_logfile_create
		echo
		echo
		echo "Domain Name is : " $domain_name:443
 	       	echo "Export Project Name is :" $project_name
 	       	echo "Updated Config file location : /work/$project_name/source_export/joblist.cfg" 
 	       	echo
 	       	echo
		echo "Confirm the above values are correct before Job Export begins (Y/N): "
		read YN
		case $YN in
			[yY]*)
                                k=0;l=0
				cat /work/$project_name/source_export/joblist.cfg | while read jobname
				do
					exportjob="-base=\"$vip_name/$project_name\" \"$jobname.*\""
					job_name=`echo $jobname | rev | cut -d/ -f1 | rev | tr ' ' '\_' | tr '/' '\_'`
					archivejob="/work/$project_name/source_export/$job_name.isx"
					print ""
					i="export -domain $domain_name:443 -u $user_id -p $password_user -archive "$archivejob" -datastage '-incexec $exportjob';"

					set +x
					echo $i > $fileset_name
				
					s2="/apps/ETL/IS/8.5/Clients/istools/cli/istool -script $fileset_name"
					echo "Exporting the job $jobname"
					eval "$s2" >> $Log_file_name
									
					J=`echo $?`

					case $J in
						0)
  							k=`expr $k + 1`
  							echo "Export completed successfully for the job: $jobname"
  							rm -f $fileset_name
  						;;
 						*)
  							l=`expr $l + 1`
  							echo "Export failed for the job: $jobname"
  							failed_config="/work/${project_name}/source_export/IndividualJob_export_failed_${export_date}.cfg"
  							echo $jobname >> $failed_config
  							rm -f $fileset_name
  						;;
					esac
				done
				ERR=$?
				if [[ $ERR -ne 0 ]] ; then
				{
					print "Failed to Export the Jobs, see errors above."
				exit 1
				}
				fi
			;;
			*)
 				f_menu_header "Individual Job Export" "Update the job export Config file and Select a Valid option"
   				REPLY=''
   				continue
  			;;
		esac
	;;
	esac
f_output_result
echo
echo
f_menu_header "istool Export Utility" "Please select an option for more Export else Quit"
fi

REPLY=''
done
}

#########################################################################
# Main Program						               ##		
#########################################################################

. /apps/ETL/IS/8.5/Server/DSEngine/dsenv

echo "Host Name is: " `hostname`

case `hostname` in
	dseax0011|dseax0021)
        	domain_name="devetl"
       		vip_name="VDSD01"
		f_export
        ;;
	dseax0013|dseax0023)
        	domain_name="testetl"
        	vip_name="VDST01"
		f_export
        ;;
        dseax0111|dseax0121)
        	domain_name="devetl11"
       		vip_name="VDSD11"
		f_export
        ;;
	dseax0113|dseax0123)
        	domain_name="testetl11"
        	vip_name="VDST11"
		f_export
        ;;
	*)
	echo "You cannot export anything from this Server. Please login into Dev/Test environment"
	break
	;;
esac

return
### End Main


