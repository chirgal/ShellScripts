#!/bin/sh                                                                           #
#Script: CreateSchema.sh                                                            # 
#Object: IP_P2P_IG_380                                                              #
#Description:                                                                       # 
#This script generates schema file from APC datafile.                               #
#It also adds the missing fields to schema file, those are required by interface.   #
#Finally script echos the count of missing fields.                                  #
#      Creator                Date                       Version                    #
#----------------------------------------------------------------                   #
#  Chirgal Hansdah         02-Feb-2013                  1.0(Initial)                #
#####################################################################################

##check for required parameteres
if [ "$1" == "" -o "$2" == "" -o "$3" == ""  ]
  then
    echo  "Usage: sh $0  <APCfilename>  <fieldname1,fieldname2,....fieldnameN> <SchemaFileName>"
    exit 2
fi	

##Get the fieldnames present in APC datafile##
apcfields=`grep 'FIELDNAMES' $1|cut -d ':' -f 2|tr ',' '\n'|sed 's/^[ \t]*//;s/[ \t]*$//'|sed 's/\( \)\{1,\}/_/g'|awk '{print toupper($0);}'`

##Get the fieldnames present in sequence parameter##
tgtfields=`echo $2|awk '{split($0, fields,",");for (i in fields) print fields[i];}'`
flag=-1

##Find the unmatched/missing fields from APC datafile.## 
for field in $tgtfields
do
 match=`echo $apcfields|grep $field'`
 if [ "$match" == "" ]
  then
   flag=`expr ${flag} + 1`
   array[${flag}]="${field}"
   #-------Debugging-------------
   #echo "No match found for field  $field "
 fi
done


#-------Debugging-------------
#if [ $flag == -1 ]
# then
#  echo "All Fields are present in Data file."
#fi
#-------Debugging-------------

##Generate schemafile for the fields present in APC data file##  
echo 'record\n  {final_delim=none, record_delim="\\r\\n",delim=",",quote=none}\n('>$3
for field in $apcfields
 do
   echo $field|awk '{printf "%s:string;\n",$0}'>>$3
 done

##Add missing fields to the end of schema file that is expected by datastage## 
for field in "${array[@]}"
 do
   echo $field|awk '{printf "%s:string;\n",$0}'>>$3
 done
echo ')'>>$3

##Output the count of missing fields that were added to schema file##
echo "${#array[*]}"
exit 0